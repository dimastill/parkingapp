﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    static class Settings
    {
        static double startBalance = 0;
        static int spaceParking = 10;
        static double periodicityPayment = 5;
        static double coefficientFine = 2.5;

        public static bool IsBlocked { get; set; } = false;
        public static double StartBalance
        {
            get
            {
                return startBalance;
            }
            set
            {
                try
                {
                    IsPositiveValue(value);
                    startBalance = value;
                }
                catch(Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }
        public static int SpaceParking
        {
            get
            {
                return spaceParking;
            }
            set
            {
                try
                {
                    IsPositiveValue(value);
                    spaceParking = value;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }
        public static double PeriodicityPayment
        {
            get
            {
                return periodicityPayment;
            }
            set
            {
                try
                {
                    IsPositiveValue(value);
                    periodicityPayment = value;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }
        public static Dictionary<string, double> Rate { get; } = new Dictionary<string, double>
        {
            { "Car", 2.0 },
            { "Truck", 5.0 },
            { "Bus", 3.5 },
            { "Motorcycle", 1.0 }
        };
        public static double CoefficientFine
        {
            get
            {
                return coefficientFine;
            }
            set
            {
                try
                {
                    IsPositiveValue(value);
                    coefficientFine = value;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }

        static void IsPositiveValue(int value)
        {
            if (value < 0)
                throw new Exception("Incorrect value");
        }

        static void IsPositiveValue(double value)
        {
            if (value < 0)
                throw new Exception("Incorrect value");
        }

        static void IsPositiveValue(Dictionary<string, double> value)
        {
            if (value.Values.Any(x => x < 0))
                throw new Exception("Incorrect value");
        }
    }
}
