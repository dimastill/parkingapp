﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    class Transaction
    {
        public Transaction()
        {
            AmountTransaction++;
        }
        public int TransactionID { get; set; }
        public DateTime Time { get; set; }
        public int IDVehicle { get; set; }
        public double Sum { get; set; }
        public static int AmountTransaction { get; set;}
    }
}
