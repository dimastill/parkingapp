﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IMenu menu = StartMenu.GetInstance();
            menu.Display();

            int indexMenuItem = menu.GetInput();

            do
            {
                IMenu nextMenu = menu.SelectMenuItem(indexMenuItem);
                nextMenu.Display();
                indexMenuItem = nextMenu.GetInput();
                menu = nextMenu;
            } while (true);
        }
    }
}
