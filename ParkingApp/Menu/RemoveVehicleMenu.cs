﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    class RemoveVehicleMenu : IMenu
    {
        private static RemoveVehicleMenu instance;

        RemoveVehicleMenu()
        { }
        public static RemoveVehicleMenu GetInstance()
        {
            if (instance == null)
                instance = new RemoveVehicleMenu();

            Console.Clear();

            return instance;
        }
        public void Display()
        {
            Console.WriteLine("Vehicles in the parking: ");

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            foreach (Vehicle vehicle in Parking.Vehicles)
                Console.WriteLine($"{vehicle.GetNameVehicle()}");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("\n1. Remove\n" +
                              "2. Back");
        }

        public int GetInput()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Select menu item: ");
            int.TryParse(Console.ReadLine(), out int selectedMenuItem);
            Console.ForegroundColor = ConsoleColor.Gray;

            return selectedMenuItem;
        }

        public IMenu SelectMenuItem(int indexMenuItem)
        {
            try
            {
                switch (indexMenuItem)
                {
                    case 1:
                        Console.Write("Select vehicle ID: ");
                        int index = Convert.ToInt32(Console.ReadLine());

                        Vehicle removeVehicle = Parking.FindVehicle(index);
                        Parking.RemoveVehicle(removeVehicle);

                        return GetInstance();
                    case 2:
                        return ParkingMenu.GetInstance();
                    default:
                        throw new Exception("Incorrect input");
                }
            }
            catch (Exception ex)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nError: {ex.Message}. Try again.\n");
                Console.ForegroundColor = ConsoleColor.Gray;
                Display();
                indexMenuItem = GetInput();
                return SelectMenuItem(indexMenuItem);
            }
        }
    }
}
