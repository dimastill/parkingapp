﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
     class StartMenu : IMenu
     {
        private static StartMenu instance;
        
        StartMenu()
        { }
        public static StartMenu GetInstance()
        {
            if (instance == null)
                instance = new StartMenu();

            Console.Clear();

            return instance;
        }

        public void Display()
        {
            Console.WriteLine("1. Start\n" +
                              "2. Settings\n" +
                              "3. Exit");
        }

        public int GetInput()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Select menu item (Hint: Enter index menu item): ");
            int.TryParse(Console.ReadLine(), out int selectedMenuItem);
            Console.ForegroundColor = ConsoleColor.Gray;

            return selectedMenuItem;
        }

        public IMenu SelectMenuItem(int indexMenuItem)
        {
            try
            {
                switch (indexMenuItem)
                {
                    case 1:
                        return ParkingMenu.GetInstance();
                    case 2:
                        return SettingsMenu.GetInstance();
                    case 3:
                        Environment.Exit(0);
                        break;
                    default:
                        throw new Exception("Incorrect input");
                }
            }
            catch (Exception ex)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nError: {ex.Message}. Try again.\n");
                Console.ForegroundColor = ConsoleColor.Gray;
                
                Display();
                
                indexMenuItem = GetInput();
                return SelectMenuItem(indexMenuItem);
            }

            return null;
        }
    }
}
