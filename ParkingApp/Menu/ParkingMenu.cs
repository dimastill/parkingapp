﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    class ParkingMenu : IMenu
    {
        private static ParkingMenu instance;

        ParkingMenu()
        { }
        public static ParkingMenu GetInstance()
        {
            if (instance == null)
            {
                instance = new ParkingMenu();
                Parking.GetInstance();
            }
            

            Console.Clear();

            return instance;
        }
        public void Display()
        {
            Console.WriteLine("1. Display current balance\n" +
                              "2. Display amount of money earned in the last minute\n" +
                              "3. Display the number of free / occupied parking places\n" +
                              "4. Display all Parking Transactions for the last minute\n" +
                              "5. Print the entire history of transactions (reading data from the file Transactions.log)\n" +
                              "6. Display a list of all Vehicles\n" +
                              "7. Add Vehicle to Parking\n" +
                              "8. Remove Vehicle from Parking\n" +
                              "9. Top up the balance of a particular Vehicle\n" +
                              "10. Back");
        }

        public int GetInput()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Select menu item: ");
            int.TryParse(Console.ReadLine(), out int selectedMenuItem);
            Console.ForegroundColor = ConsoleColor.Gray;

            return selectedMenuItem;
        }

        public IMenu SelectMenuItem(int indexMenuItem)
        {
            try
            {
                switch (indexMenuItem)
                {
                    case 1:
                        return BalanceMenu.GetInstance();
                    case 2:
                        return EarnedSumLastMinuteMenu.GetInstance();
                    case 3:
                        return SpaceParkingMenu.GetInstance();
                    case 4:
                        return TransactionLastMinuteMenu.GetInstance();
                    case 5:
                        return ReadLogMenu.GetInstance();
                    case 6:
                        return VehiclesListMenu.GetInstance();
                    case 7:
                        return AddVehicleMenu.GetInstance();
                    case 8:
                        return RemoveVehicleMenu.GetInstance();
                    case 9:
                        return TopUpBalanceVehicleMenu.GetInstance();
                    case 10:
                        return StartMenu.GetInstance();
                    default:
                        throw new Exception("Incorrect input");
                }
            }
            catch (Exception ex)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nError: {ex.Message}. Try again.\n");
                Console.ForegroundColor = ConsoleColor.Gray;
                
                Display();
                
                indexMenuItem = GetInput();
                return SelectMenuItem(indexMenuItem);
            }
        }
    }
}
