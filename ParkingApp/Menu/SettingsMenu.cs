﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    class SettingsMenu : IMenu
    {
        private static SettingsMenu instance;

        SettingsMenu()
        { }
        public static SettingsMenu GetInstance()
        {
            if (instance == null)
                instance = new SettingsMenu();

            Console.Clear();

            return instance;
        }
        public void Display()
        {
            Console.Write($"1. Change start balance ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"(Current: {Settings.StartBalance})");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.Write($"2. Change space parking ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"(Current: {Settings.SpaceParking})");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.Write($"3. Change periodicity payment ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"(Current: {Settings.PeriodicityPayment})");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.Write($"4. Change rate ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"\n\tCurrent: {DisplayRate()}");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.Write($"5. Change coefficient fine ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"(Current: {Settings.CoefficientFine})");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("6. Back");

            if (Settings.IsBlocked)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n(!) Settings are blocked because parking is running\n");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        public int GetInput()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Select menu item: ");
            int.TryParse(Console.ReadLine(), out int selectedMenuItem);
            Console.ForegroundColor = ConsoleColor.Gray;

            return selectedMenuItem;
        }

        public IMenu SelectMenuItem(int indexMenuItem)
        {
            try
            {
                switch (indexMenuItem)
                {
                    case 1:
                        Settings.StartBalance = InputInt32Value();
                        break;
                    case 2:
                        Settings.SpaceParking = InputInt32Value();
                        break;
                    case 3:
                        Settings.PeriodicityPayment = InputInt32Value();
                        break;
                    case 4:
                        (string, double) newValue = InputTurpleValue();

                        if (!Settings.Rate.Keys.Contains(newValue.Item1))
                            throw new Exception("Incorrect input");
                        Settings.Rate[newValue.Item1] = newValue.Item2;
                        break;
                    case 5:
                        Settings.CoefficientFine = InputDoubleValue();
                        break;
                    case 6:
                        return StartMenu.GetInstance();
                    default:
                        throw new Exception("Incorrect input");
                }
            }
            catch (Exception ex)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nError: {ex.Message}. Try again.\n");
                Console.ForegroundColor = ConsoleColor.Gray;
                
                Display();
                
                indexMenuItem = GetInput();
                return SelectMenuItem(indexMenuItem);
            }
            return GetInstance();
        }

        public int InputInt32Value()
        {
            Console.Write("Input new integer value: ");
            int newValue = Convert.ToInt32(Console.ReadLine());
            return newValue;
        }

        public double InputDoubleValue()
        {
            Console.Write("Input new value: ");
            double newValue = Convert.ToDouble(Console.ReadLine());
            return newValue;
        }

        public (string, double) InputTurpleValue()
        {
            (string, double) newValue;
            Console.Write("Input type vehicle: ");
            newValue.Item1 = Console.ReadLine();
            Console.Write("Input rate for vehicle: ");
            newValue.Item2 = Convert.ToDouble(Console.ReadLine());
            return newValue;
        }

        public string DisplayRate()
        {
            string resultString = String.Empty;

            foreach (var item in Settings.Rate)
                resultString += $"\n\t{item.Key}: {item.Value};";

            return resultString;
        }
    }
}
