﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp
{
    interface IMenu
    {
        void Display();
        int GetInput();
        IMenu SelectMenuItem(int indexMenuItem);
    }
}
