﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParkingApp
{
    delegate Transaction Withdraw();

    class Parking
    {
        static string pathLogFile = $"{Directory.GetCurrentDirectory()}\\Transactions.log";

        private static Parking instance;
        public static double Balance { get; set; } = Settings.StartBalance;
        public static List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
        public static List<Transaction> AllTransactions { get; set; } = new List<Transaction>();

        public static Withdraw withdraw;

        Parking()
        {
            TimeSpan periodSeconds = TimeSpan.FromSeconds(Settings.PeriodicityPayment);
            Timer timer = new Timer(e => CreateTransactions(), null, TimeSpan.Zero, periodSeconds);

            File.WriteAllText(pathLogFile, string.Empty);
        }

        public static Parking GetInstance()
        {
            if (instance == null)
            {
                instance = new Parking();

                Settings.IsBlocked = true;
            }

            return instance;
        }

        public static bool AddVehicle(Vehicle vehicle)
        {
            if (Vehicles.Count >= Settings.SpaceParking)
                throw new Exception("Parking is full");

            withdraw += vehicle.WithdrawMoney;

            Vehicles.Add(vehicle);
            return true;
        }

        public static void RemoveVehicle(Vehicle vehicle)
        {
            int id = vehicle.ID;

            Vehicles.Remove(vehicle);

            for (int i = id; i < Vehicles.Count; i++)
                Vehicles[i].ID = i;
        }

        public static Vehicle FindVehicle(int index)
        {
            return Vehicles.ElementAt(index);
        }

        public static void CreateTransactions()
        {
            List<Transaction> LastTransactions = new List<Transaction>();

            if (withdraw?.GetInvocationList().Length > 0)
            {
                Delegate[] delegates = withdraw?.GetInvocationList();
                foreach (Delegate del in delegates)
                    LastTransactions.Add(((Withdraw)del)());

                AllTransactions.AddRange(LastTransactions);

                Logging(LastTransactions);
            }
        }

        public static List<Transaction> FindTransactionsLastMinute()
        {
            return AllTransactions.Where(transaction => 
                                         transaction.Time >= DateTime.Now.AddMinutes(-1.0))
                                         .ToList();
        }

        public static double GetMoneyEarnedLastMinute()
        {
            List<Transaction> transactionsLastMinute = FindTransactionsLastMinute();
            return transactionsLastMinute.Sum(transaction => transaction.Sum);
        }

        public static void Logging(List<Transaction> transactions)
        {
            using (StreamWriter file = new StreamWriter(pathLogFile, true))
            {
                foreach (Transaction transaction in transactions)
                    file.WriteLine($"Transaction #{transaction.TransactionID} ({transaction.Time})\n" +
                        $"\tVehicle ID: {transaction.IDVehicle}. +{transaction.Sum}$");
            }
        }

        public static void ReadLog()
        {
            using (StreamReader file = new StreamReader(pathLogFile))
            {
                Console.WriteLine(file.ReadToEnd());
            }
        }
    }
}
